$(document).ready(function () {

    const newHeaderTextSwiper = new Swiper('.swiper-container.heading_new_pontis__text-slider', {
        slidesPerView: 'auto',
        loop: true,
        noSwiping: true,
        allowTouchMove: false,
        effect: "fade",
        on: {
            slideChange: function (swiper) {
                $(swiper.$wrapperEl[0]).find('span').removeClass('animation')
                setTimeout(function(){
                    $(swiper.$wrapperEl[0]).find('.swiper-slide-active span').addClass('animation')
                }, 50)
            },
        },
    });

    const newHeaderImgSwiper = new Swiper('.swiper-container.heading_new_pontis__img-slider', {
        loop: true,
        speed: 300,
        effect: 'fade',
        autoplay: {
            delay: 5000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.heading_new_pontis__img-slider-pagination.swiper-pagination',
            type: 'bullets',
            clickable: true,
          },
        on: {
            slideChange: function (swiper) {
                let totalSlides = swiper.slides.length - 2;
                let currentCount = (swiper.activeIndex - 1) % (totalSlides) + 1;
                let timeOut;
                if(currentCount == totalSlides){
                    swiper.params.speed = 2000;
                    timeOut = 2000;
                }else{
                    swiper.params.speed = 1000;
                    timeOut = 1000;
                }
                setTimeout(function(){
                    $(swiper.$wrapperEl[0]).find('.swiper-img').removeClass('recently-added-animation');
                    $(swiper.$wrapperEl[0]).find('.swiper-slide-active .swiper-img').addClass('animation').addClass('recently-added-animation');
                }, 50)
                setTimeout(function(){
                    $(swiper.$wrapperEl[0]).find('.swiper-img:not(.recently-added-animation)').removeClass('animation');
                }, timeOut)
            },
        },
    });

    newHeaderImgSwiper.controller.control = newHeaderTextSwiper;
});